import React from "react";
import {
  Container,
  Row,
  Form,
  InputGroup,
  FormControl,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import { useState, useEffect } from "react";
import DataSet from "../data.csv";
import Papa from "papaparse";
import ShopCard from "./ShopCard";

export default function CatalogPage() {
  const [data, setData] = useState([]);
  const [search, setSearch] = useState(" ");
  const [type, setType] = useState(" ");

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(DataSet);
      const reader = response.body.getReader();
      const result = await reader.read();
      const decoder = new TextDecoder("utf-8");
      const csvData = decoder.decode(result.value);
      const parsedData = Papa.parse(csvData, {
        header: true,
        skipEmptyLines: true,
      }).data;
      setData(parsedData);
    };
    fetchData();
  }, []);

  return (
    <>
      {data.length ? (
        <Container className="">
          <Form className="m-4">
            <InputGroup>
              <InputGroup.Text id="title">Search</InputGroup.Text>
              <FormControl
                placeholder="Title"
                onChange={(t) => setSearch(t.target.value)}
              />
              <DropdownButton
                variant="outline-secondary"
                title="All"
                id="input-group-dropdown-2"
                align="end"
              >
                <Dropdown.Item onClick={() => setType(" ")}>All</Dropdown.Item>
                <Dropdown.Item onClick={() => setType("Apparel")}>
                  Apparel
                </Dropdown.Item>
                <Dropdown.Item onClick={() => setType("Pop!")}>
                  Pop!
                </Dropdown.Item>
                <Dropdown.Item onClick={() => setType("Keychains")}>
                  Keychains
                </Dropdown.Item>
              </DropdownButton>
            </InputGroup>
          </Form>
          <Row xs={2} lg={3} xxl={4} className="justify-content-center m-1">
            {data
              .filter((item) => {
                return search.toLowerCase() === ""
                  ? item
                  : item.title.toLowerCase().includes(search);
              })
              .filter((item) => {
                return type === "" ? item : item.product_type.includes(type);
              })
              .map((item) => (
                <ShopCard key={item.uid} item={item} />
              ))}
          </Row>
        </Container>
      ) : null}
    </>
  );
}
