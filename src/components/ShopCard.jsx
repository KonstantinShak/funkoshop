import React from "react";
import { Card, Col, ButtonGroup, ToggleButton } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ShopCard({ item }) {
  return (
    <Col key={item.uid} className="m-3">
      <Card style={{ width: "18rem", height: "30rem" }}>
        <Link
          to={item.uid}
          state={{ item: item }}
          style={{ textDecoration: "none", color: "black" }}
        >
          <Card.Img variant="top" src={item.img} />
          <Card.Body>
            <Card.Title
              style={{
                maxHeight: "3rem",
                textOverflow: "ellipsis",
                overflow: "hidden",
                fontSize: "18px",
              }}
            >
              {item.title}
            </Card.Title>
            <Card.Text className="mt-3 text-muted">
              {item.product_type}
            </Card.Text>
          </Card.Body>
        </Link>
        <ButtonGroup
          className="m-3"
          style={{ position: "absolute", bottom: "0", width: "90%" }}
        >
          <ToggleButton key="0" variant="outline-success disabled">
            {item.price}
          </ToggleButton>
          <ToggleButton key="1" type="checkbox" variant="success">
            Add to cart
          </ToggleButton>
        </ButtonGroup>
      </Card>
    </Col>
  );
}
