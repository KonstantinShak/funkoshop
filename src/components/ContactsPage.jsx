import React from "react";
import { Card, Container } from "react-bootstrap";

export default function ContactsPage() {
  return (
    <Container className="mb-4">
      <Card className="text-center" style={{ height: "48rem" }}>
        <Card.Header>Contacts</Card.Header>
        <Card.Body>
          <Card.Title>Shakhmin Konstantin</Card.Title>
          <Card.Text>Some small test expirience for big company</Card.Text>
        </Card.Body>
      </Card>
    </Container>
  );
}
