import React from "react";
import {
  Container,
  Row,
  Col,
  Image,
  ButtonGroup,
  ToggleButton,
} from "react-bootstrap";
import { useLocation } from "react-router-dom";

export default function ProductPage() {
  const { state } = useLocation();
  const item = state.item;

  return (
    <>
      <Container className="">
        <Row>
          <Col xs={12} md={8}>
            <Image src={item.img} thumbnail />
          </Col>
          <Col xs={6} md={4}>
            <h1>{item.title}</h1>
            <h5>
              <div dangerouslySetInnerHTML={{ __html: item.description }} />
            </h5>
            <ButtonGroup>
              <ToggleButton key="0" variant="outline-success disabled">
                {item.price}
              </ToggleButton>
              <ToggleButton key="1" type="checkbox" variant="success">
                Add to cart
              </ToggleButton>
            </ButtonGroup>
          </Col>
        </Row>
      </Container>
    </>
  );
}
