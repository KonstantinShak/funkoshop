import React from "react";
import { Container, Carousel, Image } from "react-bootstrap";
import LogoImage from "../images/Funko-Logo.wine.svg";
import SampleImage from "../images/sample.jpg";
import SampleImage2 from "../images/sample2.jpg";

export default function HomePage() {
  return (
    <>
      <Container>
        <h1 className="text-center">Popular products</h1>
        <h6 className="text-center text-muted">(random)</h6>
        <Carousel data-bs-theme="dark" className="text-center">
          <Carousel.Item interval={1200}>
            <Image height="800px" src={LogoImage} />
            <Carousel.Caption>
              <h3>This is a test Funko poject</h3>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item interval={1500}>
            <Image height="800px" src={SampleImage} />
            <Carousel.Caption>
              <h3>You can check full Funko cotalog</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <Image height="800px" src={SampleImage2} />
            <Carousel.Caption>
              <h3>Enjoy!</h3>
              <p>
                Praesent commodo cursus magna, vel scelerisque nisl consectetur.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </Container>
    </>
  );
}
