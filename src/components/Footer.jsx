import React from "react";
import { Col, Container, Navbar } from "react-bootstrap";
import { BsYoutube } from "react-icons/bs";

export default function Footer() {
  return (
    <>
      <footer>
        <Navbar bg="dark" data-bs-theme="dark" style={{ width: "100%" }}>
          <Container fluid className="justify-content-center text-center">
            <Col xs={5}>
              <h6 style={{ color: "white" }}>
                This is a very nice footer. Vozmite na rabotu, please. 2023.
              </h6>
              <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">
                <BsYoutube color="white" />
              </a>
            </Col>
          </Container>
        </Navbar>
      </footer>
    </>
  );
}
