import React from "react";
import { Container, Nav, Navbar, Badge } from "react-bootstrap";
import { BsBag } from "react-icons/bs";
import logo from "../images/Funko-Logo.wine.svg";

export default function NaviBar() {
  return (
    <Navbar sticky="top" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand href="/">
          <img
            src={logo}
            width="100"
            height="30"
            className="d-inline-block align-top"
            alt="Funko"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/catalog">cotalog</Nav.Link>
            <Nav.Link href="/contacts">contacts</Nav.Link>
          </Nav>
          <Nav>
            <Container
              onClick={() => {
                console.log("11");
              }}
            >
              <BsBag size={25} />
              <Badge
                pill
                bg="success"
                className="position-absolute translate-middle"
              >
                10
              </Badge>
            </Container>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
