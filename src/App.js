import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import NaviBar from "./components/NaviBar.jsx";
import HomePage from "./components/HomePage.jsx";
import CatalogPage from "./components/CatalogPage.jsx";
import ContactsPage from "./components/ContactsPage.jsx";
import Footer from "./components/Footer.jsx";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ProductPage from "./components/ProductPage.jsx";

function App() {
  return (
    <>
      <Router>
        <NaviBar />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="catalog" element={<CatalogPage />} />
          <Route path="catalog/:id" element={<ProductPage />} />
          <Route path="contacts" element={<ContactsPage />} />
        </Routes>
        <Footer />
      </Router>
    </>
  );
}

export default App;
